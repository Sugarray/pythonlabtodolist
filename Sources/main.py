from PySide.QtGui import QWidget, QApplication
import sys
from todolist_slots import MainWindowSlots

__author__ = 'Volodavets Vladimir'


class MainWindow(MainWindowSlots):
    """
    Main class in program.
    Starts Configurator window, connects slots and displays a window.
    """

    def __init__(self, form):
        self.setupUi(form)
        self.show_days()
        self.connect_slots()

    def connect_slots(self):
        """Connects all user slots"""

        self.AddDay.clicked.connect(lambda: self.add_day(self.NewDate.text()))

        self.RemoveDay.clicked.connect(self.remove_day)

        self.RemoveTask.clicked.connect(self.remove_task)

        self.AddTask.clicked.connect(lambda: self.add_task_connect())

        self.DayList.itemClicked.connect(lambda: self.show_day_tasks(
            self.DayList.currentItem().text()))

        self.TaskList.itemClicked.connect(
            lambda: self.show_task_info(self.DayList.currentItem().text(),
                                        self.TaskList.currentItem().text()))

        return None

    def add_task_connect(self):
        try:
            date = self.DayList.currentItem().text()
            desc = self.Description.text()
            deadline = self.Deadline.text()
            combovalue = self.Importance.currentText()
            self.add_task(date, (desc, deadline, combovalue))
        except AttributeError:
            self.show_msg_box("Error", "Cannot execute add task operation.\n "
                                       "Make sure if you selected day")


class MainWidget(QWidget):
    """
    Class with overridden close event for exporting ToDo List object in redis
    """

    def closeEvent(self, event):
        ui.manager.redis_export()
        event.accept()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    window = MainWidget()

    ui = MainWindow(window)

    window.show()

    sys.exit(app.exec_())
