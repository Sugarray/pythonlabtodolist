import pickle
import redis
from todolist import ToDoList

__author__ = 'Vladimir Volodavets'


class Manager:
    """
    Class that manages ToDo List, provides redis importing and exporting.

    Note: Redis can store 512m string in key, gonna be fine.
    """

    def __init__(self):
        pass

    def connect(self):
        self.r_server = redis.Redis('localhost')

    def redis_import(self):
        """Imports serialized ToDoList object"""
        binaries = self.r_server.get("todo_list")

        if binaries is None:
            self.todo_list = ToDoList()
        else:
            self.todo_list = pickle.loads(binaries)

    def redis_export(self):
        """Exports serialized ToDoList object"""

        binaries = pickle.dumps(self.todo_list)
        self.r_server.set("todo_list", binaries)
        self.r_server.save()
