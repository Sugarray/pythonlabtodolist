from pysideui import Ui_MainWindow

__author__ = 'Volodavets Vladimir'

"""
User slots
"""


class MainWindowSlots(Ui_MainWindow):
    def __init__(self):
        super(MainWindowSlots, Ui_MainWindow).__init__()

    def add_day(self, date):
        """
        Adds day and reflects it on gui

        :param date: Date of day
        """

        try:
            self.manager.todo_list.add_day(date)
            self.DayList.addItem(date)

            self.show_msg_box("Success", "Day with date " + date +
                              " has been successfully added")

        except ValueError as e:
            self.msgbox.setText(e.__str__())
            self.msgbox.exec_()

    def remove_day(self):
        """Removes day and reflects it on gui"""

        item = self.DayList.currentItem()

        if item is None:
            return

        self.DayList.takeItem(self.DayList.row(item))
        self.TaskList.clear()
        self.manager.todo_list.remove_day(item.text())

        self.show_msg_box("Success", "Day with date " + item.text() +
                          " has been successfully removed")

    def add_task(self, date, args):
        """
        Adds task to day and reflect it on gui

        :param date: Date of day

        :param args: Task class arguments

        """
        try:
            day = self.manager.todo_list.get_day_by_date(date)
            day.add_task(args)
            self.show_msg_box("Success", "Task has been successfully added "
                                         "to " + date)

        except ValueError as e:
            print(e.__str__())
            self.show_msg_box("Info", e.__str__())

    def remove_task(self):
        """Removes task from day and reflects it on gui"""

        day_item = self.DayList.currentItem()
        item = self.TaskList.currentItem()

        if item is None:
            return

        self.TaskList.takeItem(self.TaskList.row(item))
        self.TodoInfo.clear()
        day = self.manager.todo_list.get_day_by_date(day_item.text())
        task_id = item.text().split("#")[1]
        day.remove_task(int(task_id))

        self.show_msg_box("Success", "Task has been successfully removed "
                                     "from " + day_item.text())

    def show_day_tasks(self, date):
        """Shows tasks by clicking the day"""

        day = self.manager.todo_list.get_day_by_date(date)

        self.TaskList.clear()
        self.TodoInfo.clear()

        for i in day.tasks:
            self.TaskList.addItem("Task #" + str(i.task_id))

    def show_task_info(self, date, str_task_id):
        """Shows task info by clicking on it"""

        task_id = str_task_id.split("#")[1]
        day = self.manager.todo_list.get_day_by_date(date)
        task = day.get_task_by_id(int(task_id))

        self.TodoInfo.setText(task.__str__())

    def show_days(self):
        """In case we import class from redis we need to reflect all recorded
        days.
        """

        if len(self.manager.todo_list.days) != 0:
            for i in self.manager.todo_list.days:
                self.DayList.addItem(i.date)

    def show_msg_box(self, title, body):
        """QMessageBox Wrapper

        :param title: Message box title

        :param body: Message box body
        """

        self.msgbox.setWindowTitle(title)
        self.msgbox.setText(body)
        self.msgbox.exec_()
