PySide==1.2.2
argparse==1.2.1
coverage==3.7.1
nose==1.3.6
redis==2.10.3
wsgiref==0.1.2
