import unittest
from Sources.day import Day
from Sources.manager import Manager
from Sources.task import Task
from Sources.todolist import ToDoList

__author__ = 'Vladimir Volodavets'

"""
Testing TodoList Class functionality
"""

NORMAL_DAY_PATTERN = "2015-04-03"
TASK_ARGS = ("Some task", "23:15", "Optional")
TASK_ID = 1
REDIS_KEY_NAME = "todo_list"


class ToDoListTesting(unittest.TestCase):

    todo_list = ToDoList()

    def test_add_day(self):
        self.todo_list.add_day(NORMAL_DAY_PATTERN)
        self.assertIsNotNone(self.todo_list.days[0])

    def test_get_day_by_date(self):
        day = self.todo_list.get_day_by_date(NORMAL_DAY_PATTERN)
        self.assertIsInstance(day, Day)

    def test_remove_day(self):
        self.todo_list.remove_day(NORMAL_DAY_PATTERN)
        self.assertEqual(self.todo_list.days.__len__(), 0)


class DayTesting(unittest.TestCase):

    day = Day(NORMAL_DAY_PATTERN)

    def test_add_task(self):
        self.day.add_task(TASK_ARGS)
        self.assertIsNotNone(self.day.tasks)

    def test_get_task_by_id(self):
        task = self.day.get_task_by_id(TASK_ID)
        self.assertIsInstance(task.__str__(), str)

    def test_remove_task(self):
        self.day.remove_task(TASK_ID)
        self.assertEqual(self.day.tasks.__len__(), 0)


class ConnectionTesting(unittest.TestCase):

    manager = Manager()

    def test_redis_connection(self):
        self.manager.connect()
        self.assertIsNotNone(self.manager.r_server)

    # def test_redis_import_export(self):  # not working for some reason (import issue)
    #     self.manager.redis_import()
    #     self.assertIsInstance(self.manager.todo_list, ToDoList)
    #
    #     self.manager.redis_export()
    #     self.assertTrue(self.manager.r_server.exists(REDIS_KEY_NAME))


if __name__ == "__main__":
    unittest.main()